# BI-PST homework

## 1

59 jednotlivych udaju, 24 perished, 35 survived

### Skupina 1 perished

- EX: 727.9167?
- var: 106?
- med: 738?

### Skupina 2 survived

- EX: 738?
- var: 93?
- med: 739?

## 2

hist(case0201[case0201\$StatusId == 0,][,1], main="Skupina 1 - Perished", xlab="Humerus length")

F <- ecdf(case0201[case0201\$StatusId == 0,][,1])

plot(F, main="Skupina 1 - Perished")



png("s2.png")

dev.off()

### Skupina 1 perished

![histogram skupina 1](./hist_1.png)
![dist funkce skupina 1](./emp_dist_f_1.png)

### Skupina 2 survived

![histogram skupina 2](./hist_2.png)
![dist funkce skupina 2](./emp_dist_f_2.png)


## 3
### Exponential
PerishedLengths = case0201[case0201$Status == "Perished",][,1]
sum(PerishedLengths) / length(PerishedLengths)

SurvivedLengths = case0201[case0201$Status == "Survived",][,1]
sum(SurvivedLengths) / length(SurvivedLengths)


### Uniform
Hustota je 1/(b-a) pro x z <a,b> a 0 jinde. 

Spolehlivost tedy bude (1/(b-a))^n. Pro maximální spolehlivost se snažíme minimalizovat (b-a).

Protože všechny hodnoty musejí ležet mezi a a b, parametr b tedy bude max({x1,x2...xn}) a a bude min({x1,x2,...xn}).

![histogram perished](./perished_unif.png)
![histogram survived](./survived_unif.PNG)

## 5

#### mean...výběrový průměr

#### sd...směrodatná odchylka


mean_perished <- mean(data_perished)


sd_perished <- sd(data_perished)


n_perished<-length(data_perished)


mean_perished


 -- 727.9167
 
 sd_perished
 
-- 23.54259

 n_perished
 
-- 24

#### Podle vzorečku levá a pravá hranice:

left_perished <- mean_perished-(qnorm(0.975)*sd_perished/sqrt(n_perished))

left_perished

-- 718.4978

right_perished<-mean_perished+(qnorm(0.975)*sd_perished/sqrt(n_perished))

right_perished

--737.3355

#### Stejne pro "survived", vyjde:

left: 731.4274

right: 744.5726

